import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // data 
    list : [
      {seq: 1, imgPath : 'sampleImg.png', title : '아이용 트램폴린', locate : '서울시 동작구', price : '75000', wrDtm : '20200102', likeYn : 'Y', likeCnt : '0'},
      {seq: 2, imgPath : 'sampleImg.png', title : '아이용 트램폴린', locate : '서울시 동작구', price : '75000', wrDtm : '20200102', likeYn : 'Y', likeCnt : '0'},
    ]
  },
  getters: {
    // computed state의 값 을 가져오는 함수
    allListCount : function(state) {
      return state.list.length
    },
    countOfSeq : function(state) {
      let count = 0;
      state.list.forEach(one => {
        if(one.seq == '1') count++
      }) 
      return count
    },
    percentOfSeq : function(state, getters) {
      return Math.round(getters.countOfSeq / getters.allListCount * 100)
    }
  },
  mutations: {
    // state 의 값을 변경 component Method에서 commit set 해준다.
    addList : function(state, payload) {
      state.list.push(payload)
    }
  },
  actions: {
    // mutations 실행, valudation check 부분이 들어간다.
    // addList: function(context) {
      // context.commit('addList')
      addList : function({ commit }, payload) {
        commit('addList', payload)
      }
  },
  modules: {
  }
})
